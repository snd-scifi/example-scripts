#!/usr/bin/env python3

from scifi_daq.daq import Daq
from time import sleep
import logging
import argparse
from datetime import datetime

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('run', type=int, help='Set to -1 to automatically select the run number')
  parser.add_argument('--conf', '-c', default='conf_example')
  parser.add_argument('--board-ids', '-b', type=int, nargs='+')
  args = parser.parse_args()
  conf_dir = args.conf

  logging.basicConfig(level=logging.ERROR)

  d = Daq(conf_dir, subset=args.board_ids)

  print("initialize")
  start = datetime.now()
  d.connect()
  d.initialize(args.run if args.run >= 0 else None, syncWithOrbit=False, externalReset=True)
  
  print('Send the external reset and then call start_daq_extreset.py')


if __name__ == '__main__':
  main()
