#!/usr/bin/env python3

from scifi_daq.daq import Daq
from time import sleep
import logging
import argparse
from datetime import datetime

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('run', type=int, help='Set to -1 to automatically select the run number')
  parser.add_argument('--conf', '-c', default='conf_example')
  parser.add_argument('--board-ids', '-b', type=int, nargs='+')
  args = parser.parse_args()
  conf_dir = args.conf

  logging.basicConfig(level=logging.ERROR)

  d = Daq(conf_dir, subset=args.board_ids)

  print("initialize")
  start = datetime.now()
  d.connect()
  d.initialize(args.run if args.run >= 0 else None)
  d.daqBoards.disableLeds(True)

  sleep(1)

  print('Time elapsed (initializing):', datetime.now() - start)

  # sets up the DAQ boards to accept triggers from the TTC system
  d.daqBoards.setTrigger(sources=['ttc'])

  #sets up the TTC system to send triggers when a command is received (triggers are used to check synchronization)
  d.vmeClient.setL1aTrigger(l1a_input='vme')

  d.startDaq(runNumber=(args.run if args.run >= 0 else None), saveConfiguration=True)


if __name__ == '__main__':
  main()
