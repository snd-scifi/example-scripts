#!/usr/bin/env python3

from scifi_daq.daq_board.daq_board_multi import DaqBoards
import logging
import argparse

def main():
  logging.basicConfig(level=logging.INFO)
  parser = argparse.ArgumentParser()
  parser.add_argument('--conf', '-c', default='conf_example')
  parser.add_argument('--board-ids', '-b', type=int, nargs='+')
  parser.add_argument('t1', type=int)
  parser.add_argument('t2', type=int)
  args = parser.parse_args()
  conf_dir = args.conf

  d = DaqBoards(conf_dir, subset=args.board_ids)
  print('Read configuration...')
  d.readConfiguration()
  print('Done.')
 
  d.calculateThresholds(
    methodT1='fixed',
    #methodT2='fixed',
    methodT2='rate',
    methodE='fixed',
    settingsT1={'threshold': args.t1},
    # settingsT2={'threshold': args.t2},
    settingsT2={'rateMax': args.t2},
    settingsE={'threshold': 62}
  )



if __name__ == '__main__':
  main()