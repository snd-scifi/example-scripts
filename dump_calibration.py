#!/usr/bin/env python3

from scifi_daq.daq import Daq
import logging
import argparse

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--conf', '-c', default='conf_example')
  parser.add_argument('--board-ids', '-b', type=int, nargs='+')
  args = parser.parse_args()
  conf_dir = args.conf

  logging.basicConfig(level=logging.ERROR)

  d = Daq(conf_dir, subset=args.board_ids)

  d.daqServer.connect()
  d.daqBoards.readConfiguration()
  
  d.daqServer.setBoardIds(d.daqBoards.boardIds())
  d.daqServer.setDaqServerSettings(d.configuration['daq_server'])
  d.daqServer.setRunNumber(0)

  d.daqServer.setBoardMapping(d.daqBoards.boardMapping)
  d.daqServer.setSystemConfiguration(d.configuration)
  d.daqServer.setBoardsConfiguration(d.daqBoards.getFullConfiguration(strKeys=True))
  
  d.daqServer.startDaq(overwrite=True)
  d.daqServer.stopDaq()


if __name__ == '__main__':
  main()
