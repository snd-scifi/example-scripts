#!/usr/bin/env python3

from scifi_daq.daq import Daq
import logging
import argparse
from time import sleep

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--conf', '-c', default='conf_example')
  args = parser.parse_args()
  conf_dir = args.conf

  logging.basicConfig(level=logging.ERROR)

  d = Daq(conf_dir, subset=None)
  d.vmeClient.connect()
  while True:
    d.vmeClient.generateSoftL1A()
    sleep(1)



if __name__ == '__main__':
  main()
