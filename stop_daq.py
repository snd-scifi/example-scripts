#!/usr/bin/env python3

from scifi_daq.daq import Daq
import logging
import argparse

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--conf', '-c', default='conf_example')
  parser.add_argument('--board-ids', '-b', type=int, nargs='+')
  args = parser.parse_args()
  conf_dir = args.conf

  logging.basicConfig(level=logging.ERROR)

  d = Daq(conf_dir, subset=args.board_ids)
  d.connect()

  print(d.stopDaq())


if __name__ == '__main__':
  main()
