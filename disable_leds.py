#!/usr/bin/env python3

import argparse
from scifi_daq.daq_board.daq_board_multi import DaqBoards
import logging

def main():
  logging.basicConfig(level=logging.INFO)

  parser = argparse.ArgumentParser()
  parser.add_argument('--conf', '-c', type=str, default='conf_example')
  parser.add_argument('--enable', '-e', action='store_true')
  parser.add_argument('--board-ids', '-b', type=int, nargs='+')
  args = parser.parse_args()

  d = DaqBoards(args.conf, subset=args.board_ids)
  d.connect()
  d.disableLeds(not args.enable)
  
  

if __name__ == '__main__':
  main()